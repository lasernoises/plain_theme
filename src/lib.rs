extern crate vecmath;

pub type Rect = ([i64; 2], [u64; 2]);

pub type Color = [u8; 4];

#[derive(Copy, Clone)]
pub enum RectShape {
    Square,
    Round(u64)
}

impl RectShape {
    pub fn size(&self) -> u64 {
        match *self {
            RectShape::Square => 0,
            RectShape::Round(radius) => radius,
        }
    }

    pub fn rect_min_size(&self) -> [u64; 2] {
        match *self {
            RectShape::Square => [0; 2],
            RectShape::Round(radius) => [radius * 2; 2],
        }
    }
}

#[derive(Copy, Clone)]
pub struct RectTheme {
    pub background_color: Color,
    pub foreground_color: Color,
    pub shape: RectShape,
    pub border_color: Color,
    pub border_radius: u64,
    pub padding: u64,
    pub margin: u64,
}

impl RectTheme {
    pub fn border_size(&self) -> u64 {
        self.padding + self.shape.size() + self.margin
    }

    pub fn min_size(&self) -> [u64; 2] {
        [self.border_size() * 2; 2]
    }

    pub fn without_margin(&self, rect: Rect) -> Rect {
        (
            [rect.0[0] + self.margin as i64, rect.0[1] + self.margin as i64],
            [rect.1[0] - self.margin * 2, rect.1[1] - self.margin * 2],
        )
    }

    pub fn without_border(&self, rect: Rect) -> Rect {
        let border = self.border_size();
        (
            [rect.0[0] + border as i64, rect.0[1] + border as i64],
            [rect.1[0] - border * 2, rect.1[1] - border * 2],
        )
    }
}

pub struct Theme<F> {
    pub default_rect_theme: RectTheme,
    pub rect_themes: Vec<Vec<RectTheme>>,
    pub default_text_theme: F,
    pub text_themes: Vec<Vec<F>>,
}

impl<F> Theme<F> {
    pub fn rect_theme(&self, index: (usize, usize)) -> &RectTheme {
        if self.rect_themes.len() >= 1 {
            let index_0 = index.0.min(self.rect_themes.len() - 1);
            if self.rect_themes[index_0].len() >= 1 {
                let index_1 = index.1.min(self.rect_themes[index_0].len() - 1);
                return &self.rect_themes[index_0][index_1];
            }
        }
        &self.default_rect_theme
    }

    pub fn text_theme(&mut self, index: (usize, usize)) -> &mut F {
        if self.text_themes.len() >= 1 {
            let index_0 = index.0.min(self.text_themes.len() - 1);
            if self.text_themes[index_0].len() >= 1 {
                let index_1 = index.1.min(self.text_themes[index_0].len() - 1);
                return &mut self.text_themes[index_0][index_1];
            }
        }
        &mut self.default_text_theme
    }
}

pub fn offset_rect(rect: Rect, offset: [i64; 2]) -> Rect {
    ([rect.0[0] + offset[0], rect.0[1] + offset[1]], rect.1)
}

pub fn rect_point_1(rect: Rect) -> [i64; 2] {
    [rect.0[0] + rect.1[0] as i64, rect.0[1] + rect.1[1] as i64]
}

pub fn point_in_rect(point: [i64; 2], rect: Rect) -> bool {
    let rect_point_1 = rect_point_1(rect);

    if rect.0[0] <= point[0] &&
        rect.0[1] <= point[1] &&
        rect_point_1[0] > point[0] &&
        rect_point_1[1] > point[1]
    {
        true
    } else {
        false
    }
}

pub fn max_size(size_0: [u64; 2], size_1: [u64; 2]) -> [u64; 2] {
    [size_0[0].max(size_1[0]), size_0[1].max(size_1[1])]
}

pub fn min_size(size_0: [u64; 2], size_1: [u64; 2]) -> [u64; 2] {
    [size_0[0].min(size_1[0]), size_0[1].min(size_1[1])]
}

pub fn max_point(point_0: [i64; 2], point_1: [i64; 2]) -> [i64; 2] {
    [point_0[0].max(point_1[0]), point_0[1].max(point_1[1])]
}

pub fn min_point(point_0: [i64; 2], point_1: [i64; 2]) -> [i64; 2] {
    [point_0[0].min(point_1[0]), point_0[1].min(point_1[1])]
}

pub fn rect_intersection(rect_0: Rect, rect_1: Rect) -> Rect {
    let pos = min_point(rect_0.0, rect_1.0);

    let size = vecmath::vec2_sub(
        min_point(rect_point_1(rect_0), rect_point_1(rect_1)),
        pos
    );

    (pos, [size[0] as u64, size[1] as u64])
}
